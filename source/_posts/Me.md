---
title: Me
author: Viktorii Anne
cover: /img/viktoriianne.png
tags: []
categories: []
side-image: /img/viktoriianne.png
date: 2018-09-27 01:42:00
---

Viktorii-Anne was born in France in 1991 and grew up in London. She has always loved to write and would keep diaries as a child, along with notebooks filled with imaginative stories and poetry that did not always rhyme (please note she simply adores paper).
Art and design is also a well known talent of hers as she went on to specialise in illustration. Although this lead to a very fashion focused career,
Viktorii-Anne never ever really parted from her first love as she continues to write a variety of short stories for children.

Ambisha and Bravid are inspired by her memorable childhood days. Also with her two adorable little girl and excessive number of nieces and nephews she never falls short of funny school stories and unique personalities.

Bravid and Ambisha will make children laugh and feel the silliest, while adults reminisce back to being a kid where worries were much more miniscule but still as meaningful. So with all that said please clear your throats, prepare your best reading voice and ENJOY!
